import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shuttle/model/boarding/boarding-model.dart';
import 'package:shuttle/pages/homepage.dart';
import 'package:shuttle/pages/login.dart';
import 'package:shuttle/pages/register.dart';

class BoardingPage extends StatefulWidget {
  @override
  _BoardingPageState createState() => new _BoardingPageState();
}

class _BoardingPageState extends State<BoardingPage>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final boardingModel = ScopedModel.of<BoardingModel>(context);
    return Container(
        height: MediaQuery.of(context).size.height,
        child: PageView(
          controller: boardingModel.controller,
          physics: new AlwaysScrollableScrollPhysics(),
          children: <Widget>[LoginPage(), HomePage(), RegisterPage()],
          scrollDirection: Axis.horizontal,
        ));
  }
}
