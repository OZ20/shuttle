import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shuttle/model/user/user-model.dart';
import 'package:shuttle/widget/misc-button.dart';

class MiscPage extends StatefulWidget {
  @override
  State createState() {
    return _MiscPage();
  }
}

class _MiscPage extends State<MiscPage> {
  @override
  Widget build(BuildContext context) {
    final userModel = ScopedModel.of<UserModel>(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 50.0,
        ),
        GridView.count(
          shrinkWrap: true,
          crossAxisCount: 2,
          children: <Widget>[
            MiscButton(
              'Account',
              Icons.account_circle,
              onTap: () => userModel.logout(),
            ),
            MiscButton('Account', Icons.account_circle),
            MiscButton('Account', Icons.account_circle),
            MiscButton('Account', Icons.account_circle),
          ],
        ),
      ],
    );
  }
}
