import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shuttle/model/map/map-model.dart';

class MapPage extends StatefulWidget {
  @override
  State createState() {
    return _MapsPage();
  }
}

class _MapsPage extends State<MapPage> {
  MapModel _mapModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._mapModel = ScopedModel.of(context);
  }

  @override
  Widget build(BuildContext context) {
    _mapModel.getLocation();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Buses around you'),
      ),
      body: ScopedModelDescendant<MapModel>(
        builder: (context, child, model) {
          if (model.latLng != null)
            return GoogleMap(
              markers: [
                Marker(
                  position: model.latLng,
                  markerId: MarkerId('bas1'),
                  icon: model.marker ?? BitmapDescriptor.defaultMarker
                )
              ].toSet(),
              onMapCreated: (map) => model.googleMapController = map,
              myLocationButtonEnabled: false,
              myLocationEnabled: true,
              initialCameraPosition:
              CameraPosition(zoom: 15.0, target: model.latLng),
            );
          else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.my_location),
        onPressed: () => _mapModel.setCurrentLocation(),
      ),
    );
  }
}
