import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shuttle/model/dashboard/dashboard-model.dart';
import 'package:shuttle/model/misc/misc-model.dart';
import 'package:shuttle/pages/dashboard.dart';
import 'package:shuttle/pages/misc-page.dart';

List<Widget> listWidget = [
  ScopedModel(
    model: DashboardModel(),
    child: Dashboard(),
  ),
  ScopedModel(
    model: MiscModel(),
    child: MiscPage(),
  )
];
