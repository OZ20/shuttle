import 'package:flutter/material.dart';

class ItemBar extends StatelessWidget {
  final IconData iconData;
  final GestureTapCallback onTap;

  ItemBar(this.iconData, this.onTap);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Icon(iconData),
    );
  }
}
