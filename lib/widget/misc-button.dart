import 'package:flutter/material.dart';

class MiscButton extends StatelessWidget {
  final String title;
  final IconData icon;
  final GestureTapCallback onTap;

  MiscButton(this.title, this.icon, {this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: onTap,
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: 40.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(title),
            ],
          ),
        ),
      ),
    );
  }
}
