import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class Constants {
 static FirebaseApp app;
  // realtime database
  static FirebaseDatabase database = FirebaseDatabase.instance;
  static DatabaseReference locationReference =
      database.reference().child('Location');

  // firestore
  static Firestore firestore = Firestore.instance;

  static CollectionReference get userRef => firestore.collection('Student');
}
