import 'package:shuttle/model/user/student.dart';
import 'package:shuttle/values/constant.dart';

Future addStudent(Student student) async {
  return await Constants.userRef.add(student.toJson());
}
